import Vue from "vue";
import Router from "vue-router";
import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./components/Profile.vue')
    },
    {
      path: "/movie",
      alias: "/movie",
      name: "movie-list",
      component: () => import("./components/MovieList")
    },
    {
      path: "/movies/:id",
      name: "movie-details",
      component: () => import("./components/Movie")
    },
    {
      path: "/movie-add",
      name: "movie-add",
      component: () => import("./components/AddMovie")
    },
    {
        path: "/series",
        alias: "/series",
        name: "series-list",
        component: () => import("./components/SeriesList")
      },
      {
        path: "/series/:id",
        name: "series-details",
        component: () => import("./components/Series")
      },
      {
        path: "/series-add",
        name: "series-add",
        component: () => import("./components/AddSeries")
      },
      {
        path: "/book",
        alias: "/book",
        name: "book-list",
        component: () => import("./components/BookList")
      },
      {
        path: "/book/:id",
        name: "book-details",
        component: () => import("./components/Book")
      },
      {
        path: "/book-add",
        name: "book-add",
        component: () => import("./components/AddBook")
      }
  ]
});