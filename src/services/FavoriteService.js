import axios from 'axios';
import authHeader from './AuthHeader';

const API_URL = 'http://localhost:8080/api/favorite';

class FavoriteService {

  getFavorites() {
    return axios.get(API_URL, { headers: authHeader() });
  }

  addBook(id) {
    return axios.post(`${API_URL}/book/${id}`,{},{ headers: authHeader() });
  }

  addSeries(id) {
    return axios.post(`${API_URL}/series/${id}`,{},{ headers: authHeader() });
  }

  addMovies(id) {
    return axios.post(`${API_URL}/movie/${id}`,{},{ headers: authHeader() });
  }

  deleteBook(id) {
    return axios.delete(`${API_URL}/book/${id}`,{ headers: authHeader() });
  }

  deleteSeries(id) {
    return axios.delete(`${API_URL}/series/${id}`,{ headers: authHeader() });
  }

  deleteMovies(id) {
    return axios.delete(`${API_URL}/movie/${id}`,{ headers: authHeader() });
  }

}

export default new FavoriteService();