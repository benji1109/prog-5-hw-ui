import http from "../http";
import authHeader from './AuthHeader';

class BookDataService {
  getAll() {
    return http.get("/book",  { headers: authHeader() });
  }

  get(id) {
    return http.get(`/book/${id}`,  { headers: authHeader() });
  }

  createOrUpdate(data) {
    return http.put("/book", data,  { headers: authHeader() });
  }

  delete(id) {
    return http.delete(`/book/${id}`,  { headers: authHeader() });
  }

  findByTitle(title) {
    return http.get(`/book?title=${title}`,{ headers: authHeader() });
  }

}

export default new BookDataService();