import http from "../http";
import authHeader from './AuthHeader';

class MovieDataService {
  getAll() {
    return http.get("/movie",{ headers: authHeader() });
  }

  get(id) {
    return http.get(`/movie/${id}`,{ headers: authHeader() });
  }

  createOrUpdate(data) {
    return http.put("/movie", data,{ headers: authHeader() });
  }

  delete(id) {
    return http.delete(`/movie/${id}`,{ headers: authHeader() });
  }

  findByTitle(title) {
    return http.get(`/movie?title=${title}`,{ headers: authHeader() });
  }

}

export default new MovieDataService();