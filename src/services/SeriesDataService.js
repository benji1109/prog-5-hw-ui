import http from "../http";
import authHeader from './AuthHeader';

class SeriesDataService {
  getAll() {
    return http.get("/series",  { headers: authHeader() });
  }

  get(id) {
    return http.get(`/series/${id}`, { headers: authHeader() });
  }

  createOrUpdate(data) {
    return http.put("/series", data, { headers: authHeader() });
  }

  delete(id) {
    return http.delete(`/series/${id}`,  { headers: authHeader() });
  }

  findByTitle(title) {
    return http.get(`/series?title=${title}`,{ headers: authHeader() });
  }

}

export default new SeriesDataService();